package vh.edu.tdmu.vothihongduyen.id203a010004;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity2 extends AppCompatActivity {
    private Button btnClose;
    private Button btnXoa;
    private EditText etTenNhaSx;
    private EditText etEmail;
    private EditText etDiaChi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btnClose = findViewById(R.id.btnDong);
        btnXoa = findViewById(R.id.btnXoa);
        etTenNhaSx = findViewById(R.id.etTenNhaSX);
        etEmail = findViewById(R.id.etEmail);
        etDiaChi = findViewById(R.id.etDiaChi);


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClose();
            }
        });

        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleXoa();
            }
        });
    }

    private void handleXoa() {

        etTenNhaSx.setText("");
        etEmail.setText("");
        etDiaChi.setText("");
    }

    private void handleClose() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Xác nhận để thoát");
        alertDialog.setIcon(R.drawable.ic_launcher_background);
        alertDialog.setMessage("Bạn có chắc chắn?");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Không đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.show();
    }
}