package vh.edu.tdmu.vothihongduyen.id203a010004;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Button btnClose;
    private Button btnTinhTien;
    private Button btnXoa;
    private CheckBox cbTongTien;
    private CheckBox cbCaoVoi;
    private EditText etCusName;
    private EditText etNhoRang;
    private EditText etTramRang;
    private EditText etTongTien;
    private CheckBox cbTayTrang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnClose = findViewById(R.id.btnDong);
        btnTinhTien = findViewById(R.id.btnTinhTien);
        btnXoa = findViewById(R.id.btnXoa);

        cbTongTien = findViewById(R.id.cbTayTrang);
        cbCaoVoi = findViewById(R.id.cbCaoVoi);
        cbTayTrang = findViewById(R.id.cbTayTrang);
        etCusName = findViewById(R.id.etCusName);
        etNhoRang = findViewById(R.id.etNhoRang);
        etTramRang = findViewById(R.id.etTramRang);
        etTongTien = findViewById(R.id.etTongTien);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClose();
            }
        });

        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleXoa();
            }
        });

        btnTinhTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTinhTien();
            }
        });

    }

    private void handleTinhTien() {
        long tongTien = 0;
        boolean isTayTrang = cbTayTrang.isChecked();
        boolean isCaoVoi = cbTayTrang.isChecked();
        String numNhoRang = etNhoRang.getText().toString().trim();
        String numTramRang = etTramRang.getText().toString().trim();
        if (isTayTrang) {
            tongTien += 1500000;
        }
        if (isCaoVoi) {
            tongTien += 100000;
        }
        if (!TextUtils.isEmpty(numNhoRang)) {
            int number = Integer.parseInt(numNhoRang);
            tongTien += (number * 150000);
        }

        if (!TextUtils.isEmpty(numTramRang)) {
            int number = Integer.parseInt(numTramRang);
            tongTien += (number * 200000);
        }

        etTongTien.setText(tongTien + "");
    }

    private void handleXoa() {
        cbTongTien.setChecked(false);
        cbCaoVoi.setChecked(false);
        etCusName.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
        etNhoRang.setText("");
    }

    private void handleClose() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Xác nhận để thoát");
        alertDialog.setIcon(R.drawable.ic_launcher_background);
        alertDialog.setMessage("Bạn có chắc chắn?");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Không đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.show();
    }
}